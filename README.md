# Question 1
`GEODIST StoreAppLivraison "Livreur:1" "Lieu:Place Charles Hernu" KM`

Reponse : 0km

---

# Question 2 
`GEORADIUS StoreAppLivraison 4.862881 45.770802 10000 m ASC COUNT 1`

Reponse : Livreur:1

---

# Question 3
`GEOPOS StoreAppLivraison Livreur:2`

Reponse :
```
"4.85692530870437622"  
"45.76691638150003882"
 
```
---

# Question 4
`GEORADIUS StoreAppLivraison 4.881884 45.787100 1000 m`

Reponse : Il y a 1 livreurs

---

# Question 5
`GEOADD StoreAppLivraison 4.861934 45.766797 "Livreur:6"`

---

# Question 6
`GEODIST StoreAppLivraison "Livreur:6" "Lieu:Metro Gratte-Ciel" KM`

Reponse : "1.5841" KM

---

# Question 7
## A
De base les données ne seront pas sauvegardées car Redis n'es pas persistant de base

## B
On peut activer le EDB qui clone les données sur un disque
ou le AOF qui est un journal 

---

# Question 8
Voir fichier python
